# Turning Shiny into a Desktop App

> Hello World

Shiny is popular web publishing service, unfortunately, not every application 
can be deployed on servers. This tutorial demonstrates a simple means 
by which to deploy a shiny app to desktop.

## Project

The intent is not to teach how to do complex mathematics, or to write Shiny Apps, but rather to 
demonstrate how to configure a project in the ESDC environment. It is hoped 
that this will act as a spring-board, helping users get setup quickly. 

The code itself is a simple demo app exported from RStudio. The real trick 
is to get it to run on the desktop environment.

The expectation is that teh developer wants to deploy a shared application, but is in an environment where there is no Shiny server, and is not likely to be one anytime soon. Rather than wait, the developer can take advantage of a shared folder structure, and an old feature of FireFox, to run the application on individual desktops. While not being as elegant, it represents a solution that is likely suitable for most reporting needs, and can be implemented immediately.

## Pre-Requisites

- Windows
- RStudio
- RScript
- Git
- Firefox

The demo assumes you have RStudio installed, and will be interacting with 
the system via `Powershell`.

## Feedback

Feedback on the tutorial is welcome

There are any number of things that can (and have) gone wrong with the 
tutorial:

- Typos
- Mismatched hardware
- Library upgrades

We gaurantee this configuration worked... once ... on one machine. If you 
have suggestions or changes that you think would make it better, do feel 
free to submit a merge request.

## Instructions

Steps we will be going through:

1. Checkout from GitLab
2. Configure start instructions

### Checkout base project

To get started, clone the sample project and open it in RStudio

1. Nav: https://gitlab.com/jefferey-cave/shinyapp-desktop
2. Click: Fork
3. Open your copy of the project
4. Get the clone url
5. Go to command line and checkout project

```ps1
cd ~/Project/Folder
git clone https://gitlab.com/jefferey-cave/shinyapp-desktop.git
cd desktopshinyapp
ls -al
```

You should see a listing of all the files in the project. Before we go anywhere, we should probably check to see that the project runs on our computer.

1. Double click on the file `desktopshiny.Rproj
2. Click `Run App` 

You should see the shiny app open in the in-built browser.

### Create a Starter Script

While it is great to know that the application works, it is not a great user experience. Generally, we have been called upon to create apps for those less techincially inclined, and they should not need an instruction manual to get up and running.

We an ease this by creating a starter script that loads their application from a shortcut.

The first thing to note is the output during the run of our shinyapp. When we click on the button `Run App` we see the exact `R` command being executed to acheive all of this, and its output:

```r
shiny::runApp()
```

```text
Listening on http://127.0.0.1:5436
```

Try copy/pasting your url into your local browser, you should see the same app.

Knowing that there is an `R` command that will start our application, we can now run the application without the full IDE using `rscript`

1. Stop your app in RStudio
2. Open a PowerShell terminal
3. Change to your project folder
4. Run your project using `rscript`

    ```ps1
    rscript.exe -e "shiny::runApp('.')"
    ```

Your app should be started without any interface:

```text
Listening on http://127.0.0.1:3145
```

Try pointing your browser at that new URL. You should be looking at the 
app.

> **NOTE**
> 
> The port changes every time you start. It is randomly assigned at start.

If we check the `runApp` parameters, there is one extra parameter we can 
include to make this a little more user friendly:

```ps1
rscript.exe -e "shiny::runApp('.',launch.browser=TRUE)"
```

Save that in a text file called `start.ps1`.

You now have a basic script for users to start your interactive report 

### Creating a new browser instance

Since Shiny advertises the port it is listening on, it is possible for 
us to instantiate a special browser instance on behalf of the user.

For our user, it would be nice if we were able to start a browser instance 
just for the application. We also would like to stop the Shiny instance 
when the browser stops using it.

For this example we will use Firefox as it is based on the XUL platform 
which has a well documented and modifyable inteface.

Using powershell, we can extend our shiny server starting script to listen 
for the advertised port. We can then use this advertised url/port to start 
a firefox instance.

```
& "rscript.exe" -e "shiny::runApp('.')" 2>&1 | 
    % {
		if($_ -like '*Listening on*'){
			("$_" -replace ".*Listening on ",'').Trim()
		}
	} | 
	% {
		& "C:\Program Files\Mozilla Firefox\firefox.exe" $_
	}
```

This solution gets us part way there. We are now starting a special instance 
of the browser for the shiny app. 

The issue is that when we terminate our firefox instance, our shiny instance 
must be manually stopped. 

We can continue to modify our script to start both the shiny app and the 
firefox instance seperately, and allow our script to maintain intelligence 
about them to monitor their independant process states.

```
# Start the `shiny` "thread"
$shiny = Start-Job -Name "shiny" -ArgumentList($pwd) -ScriptBlock{
	param($workingdir);
	cd $workingdir;
	& "rscript.exe" -e "shiny::runApp('.')" 2>&1 | % {
		if($_ -like '*Listening on*'){
			("$_" -replace ".*Listening on ",'').Trim()
		}
	}
} 
```

At this point Shiny is started as a `job` which is a reference we can 
use to stop the job later. This does add the trick that we need to read 
from the output stream slightly differently.

```
while ($shiny.HasMoreData -or $shiny.State -eq "Running") {     
    $url = $shiny.ChildJobs[0].output.readall()
    if($url){
        break;
    }
}
```

Now, we have the url at the script level, and can proceed to start Firefox. 
Again, we want to create it as a seperate process taht we can monitor.

```
$args = @('-profile','./profile','-new-instance',"-url `"$url`"");
$ff = Start-Process "C:\Program Files\Mozilla Firefox\firefox.exe" -ArgumentList $args -PassThru -Wait
```

This will block the script until FF stops.

You may notice some extra parameters. This forces a "new-instance" and 
a new "profile" and a "wait" until Firefox completes. This custom profile 
is used to manipulate the way Firefox appears to the user. FOr the adventurous, 
go inspect the included profile to see ways you can manipulate firefox 
to make it behave more like we want to.

Our final step is to simply stop the shiny process once the firefox process has terminated.

```
Stop-Job $shiny.Id
```

The completed script looks like:

```
$shiny = Start-Job -Name "shiny" -ArgumentList($pwd) -ScriptBlock{
    param($workingdir);
    cd $workingdir;
    & "rscript.exe" -e "shiny::runApp('.')" 2>&1 | % {
        if($_ -like '*Listening on*'){
            ("$_" -replace ".*Listening on ",'').Trim()
        }
    }
} 

while ($shiny.HasMoreData -or $shiny.State -eq "Running") {
    $url = $shiny.ChildJobs[0].output.readall()
    if($url){
        break;
    }
}
$args = @('-profile','./profile','-new-instance',"-url `"$url`"");
$ff = Start-Process "C:\Program Files\Mozilla Firefox\firefox.exe" -ArgumentList $args -PassThru -Wait
Stop-Job $shiny.Id
```

At this point you should be able to simply run the script 


```
.\run.ps1
```

and (eventually... it's a little slow) see your app running in a window.


### Creating a Shortcut

One of the issues with creating a powershell script is that regular users 
can't run it without a bit of "know-how". The easiest way to get around 
this is to create an old fashioned `BAT` file.

```
cd <working directory>
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe ".\run.ps1"
```

If you run this as a regular user, you will likely get an error. The problem 
is that Windows makes scripting unavailable to users to protect them from 
malicious scripts. In order to activate the script, you need to indicate 
that you "know what you are doing". Since we only want our users to see 
the desktop window, we may as well hide the console window while we at it.

```
cd <working directory>
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -windowstyle hidden -executionpolicy bypass ".\run.ps1"
```

Given this is a desktop application, a nice shortcut file (`lnk`) is in 
order. These allow us to specify all the same parameters as the batch file 
above, as well as an icon file, while removing all the console windows. 

1. Right-Click > `New ...` > `Shortcut`
2. Set the properties
   - Target: `C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -windowstyle hidden -executionpolicy bypass ".\run.ps1"`
   - Start in: `<working directory>`
   - `Change Icon ...`

By setting those three options, your users should be approaching a seamless 
desktop experience.

